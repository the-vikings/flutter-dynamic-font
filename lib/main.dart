import 'package:flutter/material.dart';
import 'package:flutter_app/Theme/colors.dart';
import 'package:flutter_app/Theme/font.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dynamic Text Demo',
      home: MyHomePage(title: 'Dynamic Text Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 1,
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.title, style: BackgroundThemeFonts.title),
          backgroundColor: ThemeColors.primary,
        ),
        body: Font.fontSelector(setState),
        bottomNavigationBar: TabBar(
          tabs: [Tab(icon: Icon(Icons.font_download_rounded))],
        ),
      ),
    );
  }
}
