import 'package:flutter/material.dart';

class _Colors {
  static final _primary = Color(0xff8b0000);
  static final _secondary = Colors.grey;
  static final _background = Colors.white;
}

extension ThemeColors on Colors {
  static Color get primary => _Colors._primary;
  static Color get secondary => _Colors._secondary;
  static Color get background => _Colors._background;
}
