import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Theme/colors.dart';

class Font {
  static FontSize selectedFontSize = FontSize.large;
  static double _selectedValue = 3;

  static _changeFontSize(double value) {
    switch (value.toString()) {
      case "0.0":
        selectedFontSize = FontSize.xSmall;
        break;
      case "1.0":
        selectedFontSize = FontSize.small;
        break;
      case "2.0":
        selectedFontSize = FontSize.medium;
        break;
      case "3.0":
        selectedFontSize = FontSize.large;
        break;
      case "4.0":
        selectedFontSize = FontSize.xLarge;
        break;
      case "5.0":
        selectedFontSize = FontSize.xxLarge;
        break;
      case "6.0":
        selectedFontSize = FontSize.xxxLarge;
        break;
      default:
        selectedFontSize = FontSize.medium;
    }
  }

  static Widget fontSelector(StateSetter setState) {
    Icon icon(double size) {
      return Icon(Icons.font_download_rounded,
          color: ThemeColors.primary, size: size);
    }

    return StatefulBuilder(
      builder: (BuildContext context, _) {
        return Container(
          color: Colors.black12,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    icon(24),
                    Expanded(
                      child: Slider(
                        value: _selectedValue,
                        onChanged: (value) {
                          setState(() {
                            _selectedValue = value;
                            _changeFontSize(value);
                          });
                        },
                        max: FontSize.values.length.toDouble() - 1,
                        min: 0,
                        divisions: FontSize.values.length - 1,
                        activeColor: ThemeColors.primary,
                        inactiveColor: ThemeColors.primary,
                      ),
                    ),
                    icon(42),
                  ],
                ),
              ),
              Expanded(
                child: ListView(
                  children: [
                    Column(
                      children: [
                        Text("Large Title",
                            style: PrimaryThemeFonts.largeTitle),
                        Text("Title", style: PrimaryThemeFonts.title),
                        Text("Title2", style: PrimaryThemeFonts.title2),
                        Text("Title3", style: PrimaryThemeFonts.title3),
                        Text("Headline", style: PrimaryThemeFonts.headline),
                        Text("Body", style: PrimaryThemeFonts.body),
                        Text("Callout", style: PrimaryThemeFonts.callout),
                        Text("Subhead", style: PrimaryThemeFonts.subhead),
                        Text("Footnote", style: PrimaryThemeFonts.footnote),
                        Text("Caption", style: PrimaryThemeFonts.caption),
                        Text("Caption2", style: PrimaryThemeFonts.caption2),

                        // Secondary colored Font
                        Text("Large Title",
                            style: SecondaryThemeFonts.largeTitle),
                        Text("Title", style: SecondaryThemeFonts.title),
                        Text("Title2", style: SecondaryThemeFonts.title2),
                        Text("Title3", style: SecondaryThemeFonts.title3),
                        Text("Headline", style: SecondaryThemeFonts.headline),
                        Text("Body", style: SecondaryThemeFonts.body),
                        Text("Callout", style: SecondaryThemeFonts.callout),
                        Text("Subhead", style: SecondaryThemeFonts.subhead),
                        Text("Footnote", style: SecondaryThemeFonts.footnote),
                        Text("Caption", style: SecondaryThemeFonts.caption),
                        Text("Caption2", style: SecondaryThemeFonts.caption2),

                        // Background colored Font
                        Text("Large Title",
                            style: BackgroundThemeFonts.largeTitle),
                        Text("Title", style: BackgroundThemeFonts.title),
                        Text("Title2", style: BackgroundThemeFonts.title2),
                        Text("Title3", style: BackgroundThemeFonts.title3),
                        Text("Headline", style: BackgroundThemeFonts.headline),
                        Text("Body", style: BackgroundThemeFonts.body),
                        Text("Callout", style: BackgroundThemeFonts.callout),
                        Text("Subhead", style: BackgroundThemeFonts.subhead),
                        Text("Footnote", style: BackgroundThemeFonts.footnote),
                        Text("Caption", style: BackgroundThemeFonts.caption),
                        Text("Caption2", style: BackgroundThemeFonts.caption2),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

enum FontSize { xSmall, small, medium, large, xLarge, xxLarge, xxxLarge }

/**
 * ## Size
 *  | Fonts       | xSmall  | small | medium  | large | xLarge  | xxLarge | xxxLarge  |
 *  |-------------+---------+-------+---------+-------+---------+---------+-----------|
 *  | .largeTitle | 31      | 32    | 33      | 34    | 36      | 38      | 40        |
 *  | .title      | 25      | 26    | 27      | 28    | 30      | 32      | 34        |
 *  | .title2     | 19      | 20    | 21      | 22    | 24      | 26      | 28        |
 *  | .title3     | 17      | 18    | 19      | 20    | 22      | 24      | 26        |
 *  | .headline   | 14      | 15    | 16      | 17    | 19      | 21      | 23        |
 *  | .body       | 14      | 15    | 16      | 17    | 19      | 21      | 23        |
 *  | .callout    | 13      | 14    | 15      | 16    | 18      | 20      | 22        |
 *  | .subhead    | 12      | 13    | 14      | 15    | 17      | 19      | 21        |
 *  | .footnote   | 12      | 12    | 12      | 13    | 15      | 17      | 19        |
 *  | .caption    | 11      | 11    | 11      | 12    | 14      | 16      | 18        |
 *  | .caption2   | 11      | 11    | 11      | 11    | 13      | 15      | 17        |
 *
 * ## Weight
 *  | Fonts       | xSmall    | small     | medium    | large     | xLarge    | xxLarge   | xxxLarge  |
 *  |-------------+-----------+-----------+-----------+-----------+-----------+-----------+-----------|
 *  | .largeTitle | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 *  | .title      | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 *  | .title2     | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 *  | .title3     | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 *  | .headline   | Semi-Bold | Semi-Bold | Semi-Bold | Semi-Bold | Semi-Bold | Semi-Bold | Semi-Bold |
 *  | .body       | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 *  | .callout    | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 *  | .subhead    | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 *  | .footnote   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 *  | .caption    | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 *  | .caption2   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 */

class _Font {
  // region Private Constants
  static final FontWeight _regular = FontWeight.normal;
  static final FontWeight _semiBold = FontWeight.bold;

  static final String _fontFamily = "Calibri";

  // endregion

  // region Sizing Functions
  static double _largeTitleSize() {
    switch (Font.selectedFontSize) {
      case FontSize.xSmall:
        return 31;
      case FontSize.small:
        return 32;
      case FontSize.medium:
        return 33;
      case FontSize.large:
        return 34;
      case FontSize.xLarge:
        return 36;
      case FontSize.xxLarge:
        return 38;
      case FontSize.xxxLarge:
        return 40;
    }
  }

  static double _titleSize() {
    switch (Font.selectedFontSize) {
      case FontSize.xSmall:
        return 25;
      case FontSize.small:
        return 26;
      case FontSize.medium:
        return 27;
      case FontSize.large:
        return 28;
      case FontSize.xLarge:
        return 30;
      case FontSize.xxLarge:
        return 32;
      case FontSize.xxxLarge:
        return 34;
    }
  }

  static double _title2Size() {
    switch (Font.selectedFontSize) {
      case FontSize.xSmall:
        return 19;
      case FontSize.small:
        return 20;
      case FontSize.medium:
        return 21;
      case FontSize.large:
        return 22;
      case FontSize.xLarge:
        return 24;
      case FontSize.xxLarge:
        return 26;
      case FontSize.xxxLarge:
        return 28;
    }
  }

  static double _title3Size() {
    switch (Font.selectedFontSize) {
      case FontSize.xSmall:
        return 17;
      case FontSize.small:
        return 18;
      case FontSize.medium:
        return 19;
      case FontSize.large:
        return 20;
      case FontSize.xLarge:
        return 22;
      case FontSize.xxLarge:
        return 24;
      case FontSize.xxxLarge:
        return 26;
    }
  }

  static double _headlineOrBodySize() {
    switch (Font.selectedFontSize) {
      case FontSize.xSmall:
        return 14;
      case FontSize.small:
        return 15;
      case FontSize.medium:
        return 16;
      case FontSize.large:
        return 17;
      case FontSize.xLarge:
        return 19;
      case FontSize.xxLarge:
        return 21;
      case FontSize.xxxLarge:
        return 23;
    }
  }

  static double _calloutSize() {
    switch (Font.selectedFontSize) {
      case FontSize.xSmall:
        return 13;
      case FontSize.small:
        return 14;
      case FontSize.medium:
        return 15;
      case FontSize.large:
        return 16;
      case FontSize.xLarge:
        return 18;
      case FontSize.xxLarge:
        return 20;
      case FontSize.xxxLarge:
        return 22;
    }
  }

  static double _subheadSize() {
    switch (Font.selectedFontSize) {
      case FontSize.xSmall:
        return 12;
      case FontSize.small:
        return 13;
      case FontSize.medium:
        return 14;
      case FontSize.large:
        return 15;
      case FontSize.xLarge:
        return 17;
      case FontSize.xxLarge:
        return 19;
      case FontSize.xxxLarge:
        return 21;
    }
  }

  static double _footnoteSize() {
    switch (Font.selectedFontSize) {
      case FontSize.xSmall:
      case FontSize.small:
      case FontSize.medium:
        return 12;
      case FontSize.large:
        return 13;
      case FontSize.xLarge:
        return 15;
      case FontSize.xxLarge:
        return 17;
      case FontSize.xxxLarge:
        return 19;
    }
  }

  static double _captionSize() {
    switch (Font.selectedFontSize) {
      case FontSize.xSmall:
      case FontSize.small:
      case FontSize.medium:
        return 11;
      case FontSize.large:
        return 12;
      case FontSize.xLarge:
        return 14;
      case FontSize.xxLarge:
        return 16;
      case FontSize.xxxLarge:
        return 18;
    }
  }

  static double _caption2Size() {
    switch (Font.selectedFontSize) {
      case FontSize.xSmall:
      case FontSize.small:
      case FontSize.medium:
      case FontSize.large:
        return 11;
      case FontSize.xLarge:
        return 13;
      case FontSize.xxLarge:
        return 15;
      case FontSize.xxxLarge:
        return 17;
    }
  }
  // endregion

  // region Text Styles
  static TextStyle _largeTitle(Color color) => TextStyle(
      color: color,
      fontFamily: _fontFamily,
      fontSize: _largeTitleSize(),
      fontWeight: _regular);
  static TextStyle _title(Color color) => TextStyle(
      color: color,
      fontFamily: _fontFamily,
      fontSize: _titleSize(),
      fontWeight: _regular);
  static TextStyle _title2(Color color) => TextStyle(
      color: color,
      fontFamily: _fontFamily,
      fontSize: _title2Size(),
      fontWeight: _regular);
  static TextStyle _title3(Color color) => TextStyle(
      color: color,
      fontFamily: _fontFamily,
      fontSize: _title3Size(),
      fontWeight: _regular);
  static TextStyle _headline(Color color) => TextStyle(
      color: color,
      fontFamily: _fontFamily,
      fontSize: _headlineOrBodySize(),
      fontWeight: _semiBold);
  static TextStyle _body(Color color) => TextStyle(
      color: color,
      fontFamily: _fontFamily,
      fontSize: _headlineOrBodySize(),
      fontWeight: _regular);
  static TextStyle _callout(Color color) => TextStyle(
      color: color,
      fontFamily: _fontFamily,
      fontSize: _calloutSize(),
      fontWeight: _regular);
  static TextStyle _subhead(Color color) => TextStyle(
      color: color,
      fontFamily: _fontFamily,
      fontSize: _subheadSize(),
      fontWeight: _regular);
  static TextStyle _footnote(Color color) => TextStyle(
      color: color,
      fontFamily: _fontFamily,
      fontSize: _footnoteSize(),
      fontWeight: _regular);
  static TextStyle _caption(Color color) => TextStyle(
      color: color,
      fontFamily: _fontFamily,
      fontSize: _captionSize(),
      fontWeight: _regular);
  static TextStyle _caption2(Color color) => TextStyle(
      color: color,
      fontFamily: _fontFamily,
      fontSize: _caption2Size(),
      fontWeight: _regular);
  // endregion
}

// region Public Color Font Extensions
extension PrimaryThemeFonts on TextStyle {
  static TextStyle get largeTitle => _Font._largeTitle(ThemeColors.primary);
  static TextStyle get title => _Font._title(ThemeColors.primary);
  static TextStyle get title2 => _Font._title2(ThemeColors.primary);
  static TextStyle get title3 => _Font._title3(ThemeColors.primary);
  static TextStyle get headline => _Font._headline(ThemeColors.primary);
  static TextStyle get body => _Font._body(ThemeColors.primary);
  static TextStyle get callout => _Font._callout(ThemeColors.primary);
  static TextStyle get subhead => _Font._subhead(ThemeColors.primary);
  static TextStyle get footnote => _Font._footnote(ThemeColors.primary);
  static TextStyle get caption => _Font._caption(ThemeColors.primary);
  static TextStyle get caption2 => _Font._caption2(ThemeColors.primary);
}

extension SecondaryThemeFonts on TextStyle {
  static TextStyle get largeTitle => _Font._largeTitle(ThemeColors.secondary);
  static TextStyle get title => _Font._title(ThemeColors.secondary);
  static TextStyle get title2 => _Font._title2(ThemeColors.secondary);
  static TextStyle get title3 => _Font._title3(ThemeColors.secondary);
  static TextStyle get headline => _Font._headline(ThemeColors.secondary);
  static TextStyle get body => _Font._body(ThemeColors.secondary);
  static TextStyle get callout => _Font._callout(ThemeColors.secondary);
  static TextStyle get subhead => _Font._subhead(ThemeColors.secondary);
  static TextStyle get footnote => _Font._footnote(ThemeColors.secondary);
  static TextStyle get caption => _Font._caption(ThemeColors.secondary);
  static TextStyle get caption2 => _Font._caption2(ThemeColors.secondary);
}

extension BackgroundThemeFonts on TextStyle {
  static TextStyle get largeTitle => _Font._largeTitle(ThemeColors.background);
  static TextStyle get title => _Font._title(ThemeColors.background);
  static TextStyle get title2 => _Font._title2(ThemeColors.background);
  static TextStyle get title3 => _Font._title3(ThemeColors.background);
  static TextStyle get headline => _Font._headline(ThemeColors.background);
  static TextStyle get body => _Font._body(ThemeColors.background);
  static TextStyle get callout => _Font._callout(ThemeColors.background);
  static TextStyle get subhead => _Font._subhead(ThemeColors.background);
  static TextStyle get footnote => _Font._footnote(ThemeColors.background);
  static TextStyle get caption => _Font._caption(ThemeColors.background);
  static TextStyle get caption2 => _Font._caption2(ThemeColors.background);
}

// endregion
