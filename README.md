# Dynamic Fonts & Theme Example

This is an prototype example of how a Theme can be created with in a Flutter Application

[Theme Demo Video](https://youtu.be/ilrDq648TGc)

# Fonts
 #### Size
 
 | Fonts       | xSmall  | small | medium  | large | xLarge  | xxLarge | xxxLarge  |
 |-------------|---------|-------|---------|-------|---------|---------|-----------|
 | .largeTitle | 31      | 32    | 33      | 34    | 36      | 38      | 40        |
 | .title      | 25      | 26    | 27      | 28    | 30      | 32      | 34        |
 | .title2     | 19      | 20    | 21      | 22    | 24      | 26      | 28        |
 | .title3     | 17      | 18    | 19      | 20    | 22      | 24      | 26        |
 | .headline   | 14      | 15    | 16      | 17    | 19      | 21      | 23        |
 | .body       | 14      | 15    | 16      | 17    | 19      | 21      | 23        |
 | .callout    | 13      | 14    | 15      | 16    | 18      | 20      | 22        |
 | .subhead    | 12      | 13    | 14      | 15    | 17      | 19      | 21        |
 | .footnote   | 12      | 12    | 12      | 13    | 15      | 17      | 19        |
 | .caption    | 11      | 11    | 11      | 12    | 14      | 16      | 18        |
 | .caption2   | 11      | 11    | 11      | 11    | 13      | 15      | 17        |
 
 #### Weight
 
 | Fonts       | xSmall    | small     | medium    | large     | xLarge    | xxLarge   | xxxLarge  |
 |-------------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|
 | .largeTitle | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 | .title      | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 | .title2     | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 | .title3     | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 | .headline   | Semi-Bold | Semi-Bold | Semi-Bold | Semi-Bold | Semi-Bold | Semi-Bold | Semi-Bold |
 | .body       | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 | .callout    | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 | .subhead    | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 | .footnote   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 | .caption    | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 | .caption2   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   | Regular   |
 
